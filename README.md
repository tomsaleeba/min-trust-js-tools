A tool to make dealing with npm dependencies less risky. Achieved by running
`yarn` commands in a docker container with host files mounted in.

Note: still very much a work in progress.

## How to use
1. clone this repo
1. add `yarn-safe` to your `PATH`
1. use `yarn-safe` any time you would use `yarn`


## In scope
- main goal: protect host system from malicious `install` scripts in npm deps
- protect host system from malicious code in npm deps that executes at runtime
- support running as many `scripts` from `package.json` as possible
- stay simple so it's easy to audit. Trust comes from understanding what you're
  running
- support custom docker images so you can include your own toolchains
- try to be as fast as "native" `yarn` commands, e.g. use cache, etc

## Todo
- make it easy to configure (maybe a "dot file" in user home)
- consider a project-level config
- provide a mechanism to pass selected env vars into the container
- allow listening on ports without adding too much risk; `--network host`
  apparentlys adds quite a bit of risk.
- look at support for `npm` and `pnpm`
- support monorepos where parent directories should be mounted/accessible
- add tests that simulate attack vectors and assert the host system was not
  affected
- test with user IDs other than `1000`
- support docker-in-docker with a flag, so you can run `yarn` scripts that do
  docker actions like building an image
- consider integrating as part of `nvm`
