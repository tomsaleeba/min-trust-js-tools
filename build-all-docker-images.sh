#!/bin/bash
set -euo pipefail
cd "$(dirname "$0")"

for curr in 12 14; do
  pushd ./node-${curr}-alpine
  docker build --tag tomsaleeba/node:${curr}-alpine-yarn-safe .
  popd
done
